package solution.bkit.onboard.common;

public final class ControllerPath {
    private ControllerPath() {};

    public static final String USER = "user";
    public static final String CREATE = "create";
    public static final String SEARCH_BY_FIRSTNAME = "search-by-firstname";
    public static final String SEARCH_BY_NAME = "search-by-name";
}
