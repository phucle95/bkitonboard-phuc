package solution.bkit.onboard.controller;

import static solution.bkit.onboard.common.ControllerPath.*;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import solution.bkit.onboard.model.dto.UserCreateRequestDto;
import solution.bkit.onboard.model.dto.UserSearchResponseDto;
import solution.bkit.onboard.service.UserService;

import java.util.List;

@RestController
@RequestMapping(USER)
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping(CREATE)
    public ResponseEntity<?> createUser(@RequestBody @Validated UserCreateRequestDto request) throws ResponseStatusException {
        userService.createUser(request);
        return ResponseEntity.ok().build();
    }

    @GetMapping(SEARCH_BY_FIRSTNAME)
    public ResponseEntity<Page<UserSearchResponseDto>> searchByFirstName(
            @RequestParam("search") String searchKey,
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "10") Integer size) {
        final Pageable pageable = PageRequest.of(page, size);
        final Page<UserSearchResponseDto> response = userService.searchByFirstName(searchKey, pageable);
        return ResponseEntity.ok().body(response);
    }

    @GetMapping(SEARCH_BY_NAME)
    public ResponseEntity<List<UserSearchResponseDto>> searchByName(@RequestParam("search") String searchKey) {
        final List<UserSearchResponseDto> response = userService.searchByName(searchKey);
        return ResponseEntity.ok().body(response);
    }
}
