package solution.bkit.onboard.model.dto;

import lombok.Builder;
import lombok.Data;
import solution.bkit.onboard.model.entity.UserEntity;

import java.util.UUID;

@Data
@Builder
public class UserSearchResponseDto {
    private final UUID id;
    private final String username;
    private final String firstName;
    private final String lastName;

    public static UserSearchResponseDto toDto(UserEntity userEntity) {
        return UserSearchResponseDto.builder()
                .firstName(userEntity.getFirstName())
                .lastName(userEntity.getLastName())
                .username(userEntity.getUsername())
                .id(userEntity.getId()).build();
    }
}
