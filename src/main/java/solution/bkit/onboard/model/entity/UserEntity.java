package solution.bkit.onboard.model.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name="users", indexes = {
        @Index(name = "user_firstname", columnList = "firstName"),
        @Index(name = "user_lastname", columnList = "lastName")
})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EntityListeners(AuditingEntityListener.class)
public class UserEntity {
    @Id
    @GeneratedValue
    private UUID id;

    @Column(nullable = false)
    @Size(min = 6, max = 64)
    private String username;

    @Column(nullable = false)
    @NotBlank
    @Size(max = 64)
    private String firstName;

    @Column(nullable = false)
    @NotBlank
    @Size(max = 64)
    private String lastName;

    @Column
    private String dateOfBirth;

    @CreatedDate
    @Column(nullable = false)
    private LocalDateTime createdDate;

}
