package solution.bkit.onboard.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import solution.bkit.onboard.model.entity.UserEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    Page<UserEntity> findByFirstNameStartsWithIgnoreCase(String searchKey, Pageable pageable);

    List<UserEntity> findByFirstNameStartsWithIgnoreCaseOrLastNameStartsWithIgnoreCase(String firstName, String lastName );

    Optional<UserEntity> findByUsername(String username);
}
