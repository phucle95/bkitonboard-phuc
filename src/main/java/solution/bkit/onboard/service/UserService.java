package solution.bkit.onboard.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import solution.bkit.onboard.model.dto.UserCreateRequestDto;
import solution.bkit.onboard.model.dto.UserSearchResponseDto;
import solution.bkit.onboard.model.entity.UserEntity;

import java.util.List;

@Service
public interface UserService {
    UserEntity createUser(UserCreateRequestDto request) throws ResponseStatusException;

    Page<UserSearchResponseDto> searchByFirstName(String search, Pageable pageable);

    List<UserSearchResponseDto> searchByName(String search);
}
