package solution.bkit.onboard.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import solution.bkit.onboard.model.dto.UserCreateRequestDto;
import solution.bkit.onboard.model.dto.UserSearchResponseDto;
import solution.bkit.onboard.model.entity.UserEntity;
import solution.bkit.onboard.repository.UserRepository;
import solution.bkit.onboard.service.UserService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public UserEntity createUser(UserCreateRequestDto request) {
        Optional<UserEntity> checkIfUsernameExist =
                userRepository.findByUsername(request.getUsername());
        if (checkIfUsernameExist.isPresent())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Username existed!");
        final UserEntity user = UserEntity.builder()
                 .username(request.getUsername())
                 .firstName(request.getFirstName())
                 .lastName(request.getLastName())
                 .dateOfBirth(request.getDateOfBirth()).build();
        return userRepository.save(user);
    }

    @Override
    public Page<UserSearchResponseDto> searchByFirstName(String searchKey, Pageable pageable) {
        Page<UserEntity> searchResult = userRepository.findByFirstNameStartsWithIgnoreCase(searchKey, pageable);

         return searchResult.map(result -> UserSearchResponseDto.toDto(result));

    }

    @Override
    public List<UserSearchResponseDto> searchByName(String searchKey) {
        List<UserEntity> searchResult = userRepository.findByFirstNameStartsWithIgnoreCaseOrLastNameStartsWithIgnoreCase(searchKey, searchKey);

        return searchResult.stream()
                .map(result -> UserSearchResponseDto.toDto(result))
                .collect(Collectors.toList());
    }
}
